---
title: Lahu
description: Lahu language project page. A collection of information about the language spoken by the Lahu hill tribe group.
keywords: lahu font unicode
date: 2011-01-01
lastmod: 2016-11-02
tags:
- lahu
- unicode
- font
- dictionary
aliases:
  - /projects/lahu/
menu:
  main:
    identifier: lahu
    weight: 20
---

The Lahu language (ISO 639-3 code ‘lhu’) is the language spoken by the Lahu
people in Burma, Thailand, China and Laos. See also the [Wikipedia
entry for Lahu](http://en.wikipedia.org/wiki/Lahu_language).

I have been struggling to learn Lahu for the past 6-7 years with some limited
success.  These are some resources I have used. Suggestions and corrections are
always welcome.

_Ngaˬ Lahu hk'awˇ henˇ chehˇ ve yaꞈ hk'aˇ haˬ jaˆ._

<!--more-->

There are not many resources around to help with this language. James Mattisof,
a linguistics professor at Berkley university, has produced a dictionary and
grammar for the language but is not for the average reader and is pretty much
useless for the Lahu themselves. Paul Lewis, a Baptist missionary, produced an
English-Lahu-Thai dictionary that is comprehensive but contains a lot of words
not used in colloquial Lahu, particularly in Northern Thailand.

## Lahu characters and fonts

Unicode version 5.1 has the necessary characters to represent the tone marks in
the Lahu written language. There are not many fonts around that actually
include them. One good resource is [SIL](http://www.sil.org) which produces at
least two nice fonts that are capable of representing these characters. Both
[CharisSIL](http://scripts.sil.org/CharisSILfont) and
[DoulosSIL](http://scripts.sil.org/DoulosSILfont) are at Unicode version 5.
Another list of fonts grouped by their character support has be made by [Alan
Wood](http://www.alanwood.net/unicode/fontsbyrange.html).

[Instructions and files for typing Lahu on BSD/Linux](keyboard/)

## Lahu-English Bible

With a little bit of script 'hackery' a version of the Lahu New Testament along
side an English version has been made. Using LaTeX the verses are synchronised
to make comparisons easier. Here is a [sample of the Lahu-English New
Testament](/files/bible_sample.pdf).

## Other Resources

- [The English-Lahu Lexicon](http://books.google.com/books?id=DpPw5oNvKyQC) by
  James A. Matisoff. While being a comprehensive collection it uses his own
  phonetic system which is unlike most others and notably unlike the actual
  written language of the Lahu.
- [Lahu publications](https://www.jw.org/lhu) are available with modern,
  accurate translations. The resources here are amazing, nearly 900 languages!
- [Lahu New Testament at the Myanmar Bible
  society](http://www.myanmarbible.com/bible/Lahu/html/index.html) can be
  useful for online study of the language. Get yourself a hard copy if you can,
  its so much easier though it is not colloqial Lahu.
