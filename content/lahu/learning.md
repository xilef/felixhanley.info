---
title: Learning Lahu
linktitle: Learning
description: A Lahu tutorial
keywords: learnin lahu language
date: 2017-02-20
tags:
- lahu
menu:
  main:
    parent: lahu
    weight: 20
---

A Lahu language tuturial, initially written to help myself learn the language.
The original [Lahu tutorial PDF is available](/files/learning_lahu.pdf) and I
will gradually migrate it here.

## Introduction

The Lahu people are a people without a country and inhabit areas within
Thailand, China, Burma, Laos and Vietnam. Their population has been estimated
at around 800,000 people and these are generally divided up into five main
subgroups:

- Black Lahu (_Laˇhuˍ naˆ_)
- Red Lahu (_Laˇhuˍ nyi_)
- White Lahu (_Laˇhuˍ hpu_)
- Shehleh (_Laˇhuˍ shehˇ lehˍ_)
- Yellow Lahu (_Laˇhuˍ shi_)

The traditional clothing of each group is the easiest way to tell them apart
and is often the reason for their particular name.

## The Language

The Lahu language is in the Tibeto-Burman group of languages and is spoken by
the Lahu people throughout the regions of China, Myanmar and Thailand. Each
subgroup of Lahu people has its own particular dialect or way of speaking.
Most, though, can understand Black Lahu (Laˇhuˍ naˆ) and this is regarded as
the "lingua franca" of the Lahu people.

## Alphabet

The written Lahu alphabet is based on the Roman alphabet with only a few
diacritics to identify tones. For this reason many people who are already
familiar with the Roman alphabet (English speakers particularly) should be able
to begin reading within a short period of time.

Several methods of writing the sounds (orthographies) in the Lahu language
exist. The _Protestant_ and _Catholic_ orthographies were, naturally, created
by missionaries. _Matisoff_ by James A. Matisoff, a linguist from Berkeley
University, California, uses a different system and may be regarded as being a
more accurate representation of the spoken language. There is also the
_Chinese_ system. Of these four main orthographies, the _Protestant_ method is
the most widely used and understood by the Lahu people themselves and is
therefore the method that is used here. See Appendix \ref{ap:phonics} for a
chart showing a comparison between the four orthographies.


The order of the alphabet generally follows the same order as that of the
English alphabet. Some references, namely Matisoff, use a method of ordering
the alphabet based on the traditional order of Sanskrit-derived languages. This
uses the method of articulation to determine the order of the letters.

## Consonants

Most consonants in Lahu should not present a problem for English speakers.
There are a few differences that should be noted.

The consonants in the Lahu language are listed in Table~\ref{tab:consonants}.

\begin{table}[ht]
	\centering
	\begin{tabular}{c|l}
		Lahu& English Equivalent\\
		\hline
		b		& same as in English\\
		ch	& same as in English\\
		c		& \emph{j} in `jaw' (the unaspirated version of `ch')\\
		d		& same as in English\\
		f		& same as in English\\
		g		& as in English\\
		g'	& a coarse 'g' or a throaty `r' like in French, a fricative\\
		h		& as in English\\
		j		& as in English\\
		k		& \emph{g} in `git' (like `\thai{ก}' in Thai)\\
		hk	& \emph{c} in `cat'\\
		k'	& \emph{c} in `coat', at the back of the throat\\
		hk'	& \emph{c} in `caught' but further back in throat\\
		l		& as in English\\
		m		& as in English\\
		n/ny& same as in English\\
		ng	& as in English\\
		p		& \emph{b} in `bat', unaspirated p or b (like the Thai `\thai{ป}')\\
		hp	& \emph{p} in `people', aspirated\\
		pf	& as in English\\
		s/sh& as in English, often \emph{sh} sounds like just \emph{s}\\
		t		& cross between `d' and `t' (like the Thai `\thai{ต}')\\
		ht	& a `t' in English\\
		v		& as in English\\
		y		& cross between a `y' and `j'\\
		\hline
	\end{tabular}
	\caption{\label{tab:consonants}Lahu Consonants}
\end{table}

## Aspirated \& Unaspirated

As with many other languages in the area of South East Asia, there is a difference between \emph{aspirated} and \emph{unaspirated} sounds. To appreciate the difference between them some have tried saying the sounds with their hand in front of their mouth. With an aspirated sound you should feel a puff of air against you hand. Unaspirated sounds should have no puff.

For example, the sounds made by the letters `p', `t' and `c' in the words \emph{spar}, \emph{star} and \emph{scar} are unaspirated and have no puff of air. In contrast, the sounds made by the letters `c', `t' and `p' in the words \emph{car}, \emph{tar} and \emph{par} are aspirated by the puff of air. It is essential that this difference in appreciated by the speaker and heard by the listener as it changes most words.

### Fricatives

These are sounds that are made by using the throat or the rear of the palate
and are primarily made using friction rather than contact. Some letters require
more fiction and some require more contact. These guttural sounds are often
difficult for native English speakers to master and use fluently.

Another (perhaps more technical) way of representing the various consonants and
their articulation is shown in table~\ref{tab:artic}.

\footnotesize
\begin{eqlist}
\item[Bilabial] Using both lips
\item[Labio-dental] Bottom lip and upper teeth
\item[Alveolar] Tongue on palate just behind teeth
\item[Alveo-palatal] Tonge on palate
\item[Velar] Tongue at rear of palate
\item[Post-velar] Using the throat
\item[Unaspirated] No puff of air (see~\ref{sec:aspiration})
\item[Asiprated] With a puff of air (see~\ref{sec:aspiration})
\item[Voiced] Using the voice box
\item[Nasal] Using the nasal cavity
\item[Lateral] Using the side of the tongue
\end{eqlist}
\normalsize

\begin{table}[ht]
	\centering\footnotesize
	\begin{tabularx}{\textwidth}{X|cccccc}
		& Bilabial & Labio\-dental & Alveolar & Alveo\-palatal & Velar & Post\-velar\\
		\hline
		Unaspirated   & p  &   & t  & c  & k  & k'\\
		Aspirated			&	hp &   & ht & ch & hk & hk'\\
		Voiced				& b  & v & d  & j  & g  & g'\\
		Nasal					& m  &   & n  &    & ng &\\
		Voiceless			&    & f &    & sh &    & h\\
		Lateral				&    &   & l  &    &    &\\
		\hline
	\end{tabularx}
	\caption{\label{tab:artic}Consonant Articulation}
\end{table}


## Vowels

Table~\ref{tab:vowels} lists the vowels used in Lahu.
\begin{table}[hbt]
	\centering
	\begin{tabular}{c|l}
		Lahu & English Equivalent\\
		\hline
		i & as in \emph{i}n\\
		e & as in g\emph{e}t\\
		eh & as above\\
		ui & like the Thai `\thai{-ือ}'\\
		eu & like the Thai `\thai{เ}-\thai{อ}'\\
		a & as in \emph{a}re\\
		u & as in c\emph{u}e\\
		o & as in g\emph{o}t\\
		aw & as in cl\emph{aw}\\
		\hline
	\end{tabular}
	\caption{\label{tab:vowels}Lahu vowels}
\end{table}

There is also a tenth vowel 'uh' that does not occur at the beginning of a
word. It only occurs with nine consonants and in these cases can actually sound
different. Table~\ref{tab:uh} shows how 'uh' is effected with various consonant
combinations.

\begin{table}[hbt]
	\centering
	\begin{tabular}{l|l|l}
		Written & Meaning & Spoken (and often written)\\
		\hline
		tcuh ve & to send & cuh ve\\
		tsuhˇ ve & to wash & chuhˇ ve\\
		tzuhˆ ve & to itch & juhˆ ve\\
		suh ve & to die & suh ve\\
		zuh ve & to sleep & yuh ve\\
		\hline
	\end{tabular}
	\caption{\label{tab:uh}The vowel 'uh'}
\end{table}

### Diphthongs

Diphthongs can easily be described as a sound made by combining two vowels
together smoothly in one syllable. For example the word `kite' in English has a
diphthong represented by the letter \emph{i}. Phonetically it could be written
`k-eye-ee-t' if said slowly.

Table~\ref{tab:diphthongs} shows the diphthongs used in Lahu. 

\begin{table}[ht]
	\centering
	\begin{tabular}{c|l}
		Lahu & English Equivalent\\
		\hline
		ai & the `y' in fl\emph{y}\\
		ao & `ou' in gr\emph{ou}ch\\
		aweh & like the `oy' in b\emph{oy}\\
		awan & said quickly as written (rare)\\
		\hline
	\end{tabular}
	\caption{\label{tab:diphthongs}Diphthongs}
\end{table}

## Tones

Tones are often one of the more difficult aspects of many Asian languages for
English speakers to master. In English the main use of tones is to indicate a
question. By raising the tone at the end of a sentence it is usually understood
to be a question. This is not the case with Lahu! Tones in Lahu are used like a
letter of the alphabet. For example, if two words have exactly the same letters
but a different tone then it is actually a different word.

Table~\ref{tab:tones} lists the 7 tones in Lahu.

\begin{table}[ht]
	\centering
	\begin{tabular}{c|l|l}
		Tone Mark & Name & Description\\
		\hline
		(no mark)& & no tone\\
		ˇ & hkawˇ mvuh & high, falling\\
		ˉ & hkawˇ mvuh taˆ & high, rising\\
		ˆ & hkawˇ mvuh cheˆ & high, clipped\\
		ˬ & hkawˇ nehˬ & low, long\\
		ˍ & hkawˇ nehˬ zuhˍ & very low, long\\
		 & hkawˇ nehˬ cheˆ & low, clipped\\
		\hline
	\end{tabular}
	\caption{\label{tab:tones}The Lahu tones}
\end{table}

To illustrate the difference a tone makes, the word \emph{laˇ} means `tiger',
\emph{laˉ} is a `saddle basket', \emph{laˆ} is the classifier for a mile
(see~\ref{sec:classifiers}) and \emph{la} is the verb `to come'. Obviously the
context can help give the correct meaning but in most cases the correct tone is
needed to be properly understood.

## Useful Expressions

You should now be able to use the following useful greetings and expressions.
Many of the words used will be discussed in the following chapters.

\begin{float-vocab}\caption{Useful Expressions}\medskip
	\begin{eqlist}
	\item[Chehˬ sha chehˬ awˍ laˇ?] How are you?
	\item[Chehˇ sha chehˇ awˍ.] I am fine.
	\item[Nawehˬ?] And you?
	\item[Hk'awˍ k'ai le?] Where are you going?
	\item[Hk'awˍ ka laˬ le?] Where have you been?
	\item[(Awˍ) caˇ peuˬ-oˬ laˇ?] Have you had a meal yet?
	\item[Awˍ caˇ-oˬ laˇ?] Have you eaten yet?
	\item[Caˇ-oˬ.] I have eaten.
	\item[Maˇ caˇ sheˍ.] I haven't eaten yet.
	\item[Tu la-oˬ laˇ?] Good morning. (Are you up yet?)
	\item[Nawˬ hk'aˬ lo tu la ve le?] Where are you from?
	\item[Nawˬ awˬ meh aˬ hto ma meh ve le?] What is your name?
	\item[Hk'a hk'e g'ai le?] Whats going on?
	\item[A yehˬ yehˬ k'ai mehˍ] Goodbye (said \emph{to} one leaving)
	\item[K'o-e shaˍ mehˍ] Goodbye (said \emph{by} one leaving)
	\item[A shu chehˇ-awˬ laˇ?] Is anyone there?
	\item[Teˇ pawˆ k'aw mawˬ da-aˍ] See you again.
	\item[A hto ma te chehˇ ve le?] What are you doing?
	\item[Awˬ bon uiˍ jaˇ] Thank you
	\item[Yoˬ] Yes
	\item[Maˇ heˆ] No
	\item[Kaˇ naˉ ui] Chapter
	\item[Kaˇ naˉ eh] Verse
	\end{eqlist}
\end{float-vocab}


