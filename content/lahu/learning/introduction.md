The Lahu people are a people without a country and inhabit areas within
Thailand, China, Burma, Laos and Vietnam. Their population has been
estimated at around 800,000 people and these are generally divided up
into five main subgroups:

Laˇhuˍ naˆ

:   Black Lahu

Laˇhuˍ nyi

:   Red Lahu

Laˇhuˍ hpu

:   White Lahu

Laˇhuˍ shehˇ lehˍ

:   Shehleh

Laˇhuˍ shi

:   Yellow Lahu

The traditional clothing of each group is the easiest way to tell them
apart and is often the reason for their particular name.

The Lahu language is in the Tibeto-Burman group of languages and is
spoken by the Lahu people throughout the regions of China, Myanmar and
Thailand. Each subgroup of Lahu people has its own particular dialect or
way of speaking. Most, though, can understand Black Lahu (Laˇhuˍ naˆ)
and this is regarded as the “lingua franca” of the Lahu people.

# Alphabet {#sec:alphabet}

The written Lahu alphabet is based on the Roman alphabet with only a few
diacritics to identify tones. For this reason many people who are
already familiar with the Roman alphabet (English speakers particularly)
should be able to begin reading within a short period of time.

Several methods of writing the sounds (orthographies) in the Lahu
language exist. The *Protestant* and *Catholic* orthographies were,
naturally, created by missionaries. *Matisoff* by James A. Matisoff, a
linguist from Berkeley University, California, uses a different system
and may be regarded as being a more accurate representation of the
spoken language. There is also the *Chinese* system. Of these four main
orthographies, the *Protestant* method is the most widely used and
understood by the Lahu people themselves and is therefore the method
that is used here. See Appendix \[ap:phonics\] for a chart showing a
comparison between the four orthographies.

The order of the alphabet generally follows the same order as that of
the English alphabet. Some references, namely Matisoff[@JAM:2006], use a
method of ordering the alphabet based on the traditional order of
Sanskrit-derived languages. This uses the method of articulation to
determine the order of the letters.

## Consonants {#sec:consonants}

Most consonants in Lahu should not present a problem for English
speakers. There are a few differences that should be noted.

The consonants in the Lahu language are listed in
Table \[tab:consonants\].

   Lahu  English Equivalent
  ------ -----------------------------------------------------------
    b    same as in English
    ch   same as in English
    c    *j* in ‘jaw’ (the unaspirated version of ‘ch’)
    d    same as in English
    f    same as in English
    g    as in English
    g’   a coarse ’g’ or a throaty ‘r’ like in French, a fricative
    h    as in English
    j    as in English
    k    *g* in ‘git’ (like ‘’ in Thai)
    hk   *c* in ‘cat’
    k’   *c* in ‘coat’, at the back of the throat
   hk’   *c* in ‘caught’ but further back in throat
    l    as in English
    m    as in English
   n/ny  same as in English
    ng   as in English
    p    *b* in ‘bat’, unaspirated p or b (like the Thai ‘’)
    hp   *p* in ‘people’, aspirated
    pf   as in English
   s/sh  as in English, often *sh* sounds like just *s*
    t    cross between ‘d’ and ‘t’ (like the Thai ‘’)
    ht   a ‘t’ in English
    v    as in English
    y    cross between a ‘y’ and ‘j’

  : \[tab:consonants\]Lahu Consonants

### Aspirated & Unaspirated {#sec:aspiration}

As with many other languages in the area of South East Asia, there is a
difference between *aspirated* and *unaspirated* sounds. To appreciate
the difference between them some have tried saying the sounds with their
hand in front of their mouth. With an aspirated sound you should feel a
puff of air against you hand. Unaspirated sounds should have no puff.

For example, the sounds made by the letters ‘p’, ‘t’ and ‘c’ in the
words *spar*, *star* and *scar* are unaspirated and have no puff of air.
In contrast, the sounds made by the letters ‘c’, ‘t’ and ‘p’ in the
words *car*, *tar* and *par* are aspirated by the puff of air. It is
essential that this difference in appreciated by the speaker and heard
by the listener as it changes most words.

### Fricatives {#sec:fricatives}

These are sounds that are made by using the throat or the rear of the
palate and are primarily made using friction rather than contact. Some
letters require more fiction and some require more contact. These
guttural sounds are often difficult for native English speakers to
master and use fluently.

Another (perhaps more technical) way of representing the various
consonants and their articulation is shown in table \[tab:artic\].

Using both lips

Bottom lip and upper teeth

Tongue on palate just behind teeth

Tonge on palate

Tongue at rear of palate

Using the throat

No puff of air (see \[sec:aspiration\])

With a puff of air (see \[sec:aspiration\])

Using the voice box

Using the nasal cavity

Using the side of the tongue

[X|cccccc]{} & Bilabial & Labiodental & Alveolar & Alveopalatal & Velar
& Postvelar\
Unaspirated & p & & t & c & k & k’\
Aspirated & hp & & ht & ch & hk & hk’\
Voiced & b & v & d & j & g & g’\
Nasal & m & & n & & ng &\
Voiceless & & f & & sh & & h\
Lateral & & & l & & &\

## Vowels {#sec:vowels}

Table \[tab:vowels\] lists the vowels used in Lahu.

   Lahu  English Equivalent
  ------ --------------------
    i    as in *i*n
    e    as in g*e*t
    eh   as above
    ui   like the Thai ‘’
    eu   like the Thai ‘-’
    a    as in *a*re
    u    as in c*u*e
    o    as in g*o*t
    aw   as in cl*aw*

  : \[tab:vowels\]Lahu vowels

There is also a tenth vowel ’uh’ that does not occur at the beginning of
a word. It only occurs with nine consonants and in these cases can
actually sound different. Table \[tab:uh\] shows how ’uh’ is effected
with various consonant combinations.

  Written    Meaning    Spoken (and often written)
  ---------- ---------- ----------------------------
  tcuh ve    to send    cuh ve
  tsuhˇ ve   to wash    chuhˇ ve
  tzuhˆ ve   to itch    juhˆ ve
  suh ve     to die     suh ve
  zuh ve    to sleep   yuh ve

  : \[tab:uh\]The vowel ’uh’

### Diphthongs {#sec:dipthongs}

Diphthongs can easily be described as a sound made by combining two
vowels together smoothly in one syllable. For example the word ‘kite’ in
English has a diphthong represented by the letter *i*. Phonetically it
could be written ‘k-eye-ee-t’ if said slowly.

Table \[tab:diphthongs\] shows the diphthongs used in Lahu.

   Lahu  English Equivalent
  ------ --------------------------------
    ai   the ‘y’ in fl*y*
    ao   ‘ou’ in gr*ou*ch
   aweh  like the ‘oy’ in b*oy*
   awan  said quickly as written (rare)

  : \[tab:diphthongs\]Diphthongs

## Tones {#sec:tones}

Tones are often one of the more difficult aspects of many Asian
languages for English speakers to master. In English the main use of
tones is to indicate a question. By raising the tone at the end of a
sentence it is usually understood to be a question. This is not the case
with Lahu! Tones in Lahu are used like a letter of the alphabet. For
example, if two words have exactly the same letters but a different tone
then it is actually a different word.

Table \[tab:tones\] lists the 7 tones in Lahu.

   Tone Mark  Name              Description
  ----------- ----------------- ----------------
   (no mark)                    no tone
       ˇ      hkawˇ mvuh        high, falling
       ˉ      hkawˇ mvuh taˆ    high, rising
       ˆ      hkawˇ mvuh cheˆ   high, clipped
       ˬ      hkawˇ nehˬ        low, long
       ˍ      hkawˇ nehˬ zuhˍ   very low, long
             hkawˇ nehˬ cheˆ   low, clipped

  : \[tab:tones\]The Lahu tones

To illustrate the difference a tone makes, the word *laˇ* means ‘tiger’,
*laˉ* is a ‘saddle basket’, *laˆ* is the classifier for a mile
(see \[sec:classifiers\]) and *la* is the verb ‘to come’. Obviously the
context can help give the correct meaning but in most cases the correct
tone is needed to be properly understood.

## Useful Expressions

You should now be able to use the following useful greetings and
expressions. Many of the words used will be discussed in the following
chapters.

How are you?

I am fine.

And you?

Where are you going?

Where have you been?

Have you had a meal yet?

Have you eaten yet?

I have eaten.

I haven’t eaten yet.

Good morning. (Are you up yet?)

Where are you from?

What is your name?

Whats going on?

Goodbye (said *to* one leaving)

Goodbye (said *by* one leaving)

Is anyone there?

See you again.

What are you doing?

Thank you

Yes

No

Chapter

Verse
