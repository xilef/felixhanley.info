Lahu follows the general pattern of *subject* *object* *verb*. The
*subject* can simply be defined as the origin of the action, the
*object* is the recipient of the action and the *verb* is the action.

Generally the verb is the most important part of any sentence and often
the subject or object are implied and can be omitted. The following
examples display some typical sentence structures (for now just observe
the structure):

\[ex:read-book\] . (sbj) (obj) (vb) ‘We read the book.’

\[ex:i-read\] Ngaˬ [g’aw ve yoˬ]{}. (sbj) (vb) ‘I read.’

\[ex:drink\] . (sbj) (obj) (vb) ‘They drink water.’

\[ex:is-here\] Chehˇ -aw. (vb) (fp) ‘\[he\] is here.’

# Particles {#sec:particles}

In examples such as those above, the subject and object are often very
clear. In example \[ex:read-book\], the book can only *be read* by us,
it cannot *read us*. In more complex sentences, however, it is necessary
to clarify various parts of the sentence such as identifying the subject
and object. Particles allow us to do this.

[Common Particles]{}

Subject particle

Object particle or indicator

(Many uses) Identifies verbs (see \[sec:verbs\]), possession
(see \[sec:possession\]) etc…

Final affirmative particle

Final emphatic request (polite)

Final emphatic request (more forceful)

## Subject & Object

Often in spoken Lahu the subject and/or object are obvious and so the
particles are not needed. This is the case in example \[ex:i-read\]
where the speaker is obviously the subject and there is no object. In
example \[ex:read-book\] the object particle *hta* would not actually
be spoken as it is implied and obvious. In written Lahu, however, it is
more acceptable to include the particles unless they are overly obvious.
Foreigners speaking Lahu are often guilty of using too many particles in
their speech. While the sentence may be correct it is not colloquial and
may seem overly formal.

lehˬ, [Li hpu]{} hta [henˇ ve yoˬ]{}. They (sbj) bible (obj) study
‘They study the bible’

\[ex:god-teaches\] lehˬ, [ngaˬ hui]{} hta [maˍ laˇ ve yoˬ]{}. God (sbj)
us (obj) teach ‘God teaches us.’

The previous examples demonstrate the use of the particles. In
example \[ex:god-teaches\] it is particularly necessary to qualify the
subject and object to get the correct sense.
