
# About This Book {#about-this-book .unnumbered}

This book is designed to get an English speaker to a basic level of
understanding of the Lahu language. It has been designed to cover the
various areas of the language in a logical order and one that allows you
to build up your knowledge throughout your course.

Some exercises have been provided to give you an opportunity to use the
information you have just studied. You will find the vocabulary needed
for the exercises and examples is defined at the beginning of each
section, prior to actually using it. Practise saying the words in the
vocabulary sections so you are familiar with them when they are used in
examples. Also be sure to use the vocabulary sections and the dictionary
at the end of the book for more comprehensive listings.

This book has hopefully been designed to be useful both short and long
term. Sufficient space has been left in the margins for notes (and
corrections!) and extensive appendices can assist you long after you
have finished the main course material.

These abbreviations are used throughout the main text and the
dictionary.

[2]{}

noun

verb

subject

object

partical (general)

final partical

classifier

polite

literally

Words placed in square brackets ‘\[ \]’ have been added to complete the
sense of sentences.
