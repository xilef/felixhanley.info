Verbs (or action words) are the main part of Lahu speech. Many complete
sentences can be composed of just a verb and a particle (see
example \[ex:is-here\]). Verbs in their infinitive form are usually
written with the particle ‘ve’ after them. This is usually separated by
a space that is purely to assist the reader.

[Basic verbs used in the following sections]{}

have, to

eat, to

drink, to

write, to

go, to

read, to

know, to

do, to

study, to

pay respect, to

# Verb negation {#sec:negation}

To negate the meaning of a verb the word ‘maˇ’ is used *before* the
verb. The particle ‘ve’ is often omitted when a verb is negated as
example \[ex:neg-verb\] shows.

\[ex:neg-verb\] maˇ cawˇ not eat

. We not have ‘We do not have \[it\].’

. She not know ‘She does not know.’

# Verb Combination

Verbs can be combined in various ways to produce more complex ideas. A
main verb can have an additional verb placed *before* it (pre-head) or
*after* it (post-head).

## Pre-head Verbs {#pre-head-verbs .unnumbered}

Pre-head verbs are often used to indicate the number, aspect, time,
limitiations etc..

g’a [cawˬ ve]{} must have

\[ex:must-go\] g’a [k’ai ve]{} must go

h’aw [k’ai ve]{} again go ‘to go again’

g’a henˇ ve must study

k’aw [hawˇ ve]{} again study ‘to study again’

Note in example \[ex:must-go\] that if the word ‘g’a’ is placed *after*
the main verb it would be a different word meaning ‘to be able to’.
Compare example \[ex:able-go\].

## Post-head Verbs {#sec:post-head .unnumbered}

‘Post-head’ verbs often indicate the direction of the main verb.

caˇ [cawˇ ve]{} eat should ‘should eat’

\[ex:neg-post-verb\] maˇ caˇ cawˇ not eat should ‘should not eat’

hpaw k’ai ve flee go ‘to flee away’

hpaw taˆ ve flee up ‘to flee to higher point’

maˇ laˬ cawˇ not come should ‘shouldn’t come’

\[ex:able-go\] k’ai g’a ve go can ‘\[I\] can go’

Remember that the verb particle ‘ve’ is omitted when the verb is
negated, as example \[ex:neg-post-verb\] shows.

# Verb Duplication

To intensify the effect of some verbs they can be duplicated. This is
quite common in Lahu as it is in Thai.

respect do

\[ex:dup-verb\] ‘many’

maˇ maˇ [cawˬ ve]{} many many have ‘to have many’

\[ex:neg-dup-verb\] maˇ maˇ maˇ cawˬ many many not have ‘to have not
many’

In example \[ex:neg-dup-verb\] the last ‘maˇ’ is the negative one. It
can also be seen that example \[ex:dup-verb\] is not negative as the
verb particle ‘ve’ is present.

You should now be able to form many sentences using just nouns, verbs
and some particles.

# Verb Tense

Verb tense in Lahu is denoted by means of suffixes. Not all tenses and
aspects[^1] in the English language can easily be expressed in Lahu so
some of the following sections combine some English language concepts.

## Present Tense

This is the ‘default’ tense for verbs.

Ngaˬ te I do

Yawˇ te He does

## Past Tense

This describes actions that occurred in the past. The word *peuˬ* is
used after the verb to signify it is in the past tense.

Ngaˬ te peuˬ I did

Nawˬ te peuˬ You did

## Perfect Tense {#sec:perfect}

Perfect aspect can be described as an action that has been completed,
for example, “I have eaten rice”. [^2] The particle *peuˬ* is used as a
suffix to denote the affirmative perfect tense.

Yawˇ [gaˬ la]{} peuˬ he arrive (par) ‘He has arrived’

Ngaˬ awˬ caˇ peuˬ I rice eat (par) ‘I have eaten rice’

To denote the negative perfect tense the particle *peuˬ* is omitted and
*maˇ … sheˍ* wraps the verb. *sheˍ* could be interpreted as ‘yet’ so it
would become “not *something* yet”.

Yawˇ maˇ [gaˬ la]{} sheˍ he not arrive yet ‘He has not arrived yet’

Ngaˬ awˬ maˇ caˇ sheˍ I rice not eat yet ‘I have not eaten \[rice\] yet’

## Future Tense {#sec:future}

To speak in the future tense simply use the particle ‘tuˬ’ *after* the
verb that is in the future but *before* any final particle such as
‘yoˬ’.

Ngaˬ li teˇ k’oˆ hta g’aw tuˬ yoˬ I book one (clf) (par) read \[will\]
(fp) ‘I will read one book’

maˇ yaw tuˬ they not speak \[will\] ‘They will not speak’

## Continuous Tense

Continuous tense in English is primarily represented by adding ‘ing’ to
the end of a verb. To indicate continuous tense in Lahu add the verb
‘chehˇ’ (to be) after the verb that is continuing. This is an example of
a post-head verb (see section \[sec:post-head\]).

Yawˇ k’ai [chehˇ ve]{} yoˬ he go \[continue\] (par) ‘He is going’

Ngaˬ ngaˆ teˇ hkeh [oˇ ve]{} hta caˇ [chehˇ ve]{} yoˬ I bird two (clf)
those (par) eat \[continue\] (par) ‘I am eating those two birds’

## Indefinite Perfect Tense

This is similar to the perfect tense (see section \[sec:perfect\])
except that it points to some more distant or less distinct time. In
English we would perhaps use the word ‘ever’ or ‘never’.

Awˉsa-hteˉliˍ-a ngaˬ maˇ k’ai jaw Australia I not go ever ‘I have never
been to Australia’

Awˉsa-hteˉliˍ-a yawˇ k’ai jaw Australia he go ever ‘He has been to
Australia’

## Imperative Mood

The imperative mood expresses commands or requests. In Lahu the
particles ‘mehˍ’ or ‘sheˍ’ are used at the *end* of the sentence. The
word ‘sheˍ’ is more forceful.

Ngaˬ hta [maˍ laˇ]{} mehˍ me (par) teach (par) ‘Teach me’

Awˬ caˇ sheˍ rice eat (par) ‘Eat rice!’

Lo-e sheˍ enter (par) ‘Enter!’

Laˬ mehˍ come (par) ‘Come’

Mui sheˍ sit (par) ‘Sit!’

Li teˇ k’oˆ hta g’aw mehˍ book one (clf) (par) read (par) ‘Read the
book’

Negative commands simply use the word ‘taˇ’ *before* the verb. As with
verb negation (see section \[sec:negation\]) the particle ‘ve’ is
omitted. The imperative particles ‘mehˍ’ and ‘sheˍ’ can also be used to
give added meaning.

Taˇ te don’t do ‘Don’t do \[that\]!’

Taˇ mui don’t sit ‘Don’t sit!’

## Participles

Participles describe a type of verb, either the action is happening now
(present participle), or the action happened in the past (past
participle). The present participle is denoted by the word ‘tcuhˉ taˍ’
added after the verb.

k’ai [tcuhˉ taˍ ve]{} go continuous ‘to go continuously’

Yawˆ dawˇ [tcuhˉ taˍ ve yoˬ]{} he think continuous ‘He is thinking’

lehˉ [laˬ ve yoˬ]{} they fight continuous and come ‘They came fighting’

[^1]: English is quite a complex language in terms of tense, mood and
    aspect. This section of verb tenses is very simplified and does not
    try to map all the available English tenses, moods and aspects to
    Lahu.

[^2]: This is actually termed the *perfect aspect in the present tense*
    (or *present perfect*) as it refers to an event that has been
    completed in the past (perfect aspect) from the viewpoint of the
    present (tense).
