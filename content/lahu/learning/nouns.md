[Nouns used in the following sections]{}

asdfsdf

Nouns make up the majority of most peoples’ vocabulary. They are the
‘names of things’. *Proper nouns* are a type of noun that identify a
unique entity such as ‘Jehovah’. These are generally capitalised in Lahu
as they are in English.

# Number

To express the idea of more than one noun in Lahu extra words need to be
used such as *teˇ hpaˍ*, *teˇ g’ui* and *hui*.

Nuˇ [teˇ hpaˍ]{} cow plural cows

Chaw hui man plural men

Yaˇ [teˇ g’uiˬ]{} child plural children

# Gender {#sec:gender}

Gender in Lahu is generally expressed using suffixes, *paˍ* for male and
*ma* for female. This is the case for humans and many animals. Some
words do not need the suffix to indicate the gender as it is already
implied, for example *hawˉ hkaˆ* for *man* and *yaˇ miˇ* for *woman*.

Nuˇ paˍ cow male bull

nuˇ ma cow female cow

va paˍ pig male boar

va ma pig female sow

# Possession {#sec:possession}

When something *belongs* to someone then the possessive particle ‘ve’ is
used *after* the pronoun (or proper noun) and *before* the thing that is
possessed. Do not confuse this particle with the particle used with
verbs (see section \[sec:verbs\]). The possesive particle is more often
used with pronouns (see section \[sec:pronouns\]) and these are used in
the following examples.

ve hpu. they (par) money ‘Their money.’

Nawˇ ve li you (par) book ‘your book’

It is often omitted in such expressions as these:

nuˇ hkehˇ cow manure cow’s manure

g’aˆ [awˬ mvuh]{} chicken feather chicken’s feather
