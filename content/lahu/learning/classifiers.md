When referring to a number of items in Lahu you are required to use
classifiers. These are not used in English but could be likened to
saying ‘three *pairs* of trousers’. The word ‘pairs’ could be termed a
classifier for this example.

[Common Classifiers]{}

for people

gerneral classifier

for sections

for animals

for books

for days

for plants

for round things e.g. fruit

for places

for groups

for pieces of things e.g. land

for ‘types’ of things

In Lahu (and Thai) there are many different classifiers that all have
specific uses. For example, there is a classifier for fruit, a
classifier for people, a classifier for ‘sections’ of things and so on.
There is also a general classifier for objects that either do not have a
specific classifier already or for instances where the classifier is not
known at the time.

Some examples of classifiers in phrases:

\[ex:classifier\] shehˆ k’oˆ (n) (number) (clf) bible three (clf) ‘Three
bibles’

li teˇ k’oˆ book one (clf) ‘one book’

teˇ g’aˇ person one (clf) ‘one person’

\[ex:section\] teˇ tawn bible in section one (clf) ‘one scripture’

In example \[ex:section\] above the classifier *tawn* is used for
sections of things. A scripture is considered a section or passage in
the bible and so the appropriate classifier needs to be used. Also note
in example \[ex:classifier\] the order in which classifiers need to be
used.

While you may think the number of classifiers would be large in actual
fact you will find yourself using just a small subset for most
situations.

See also section \[sec:distributive\] on distributive adjectives for
describing more general and indefinite amounts of nouns.
