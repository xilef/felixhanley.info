[Common Pronouns]{}

ngaˬ

nawˬ

yawˇ

ngaˬ hui

nawˬ hui

yawˇ hui

In any language the most common expressions are about either ourselves
or the people we are with. We often use pronouns for these references.
These are usually found in the position of either the subject or the
object of a sentence. In many sentences in spoken Lahu the pronouns for
*I*, *me* and *you* are not actually used but are implied by the
context.

You will notice in the vocabulary for this section that there are three
main pronouns for the first, second and third person. The plurals of
each of these are obtained by adding the work ‘hui’. Notice that Lahu
also has a pronoun for the second person plural which is not present in
(correct!) English.
