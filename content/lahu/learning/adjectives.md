Adjectives are words or phrases that describe a noun. So in English we
would have ‘a *red* dog’ or ‘a *yellow* door’. There is no strict rule
as to whether the adjective goes before or after the noun.

# Proper

Proper adjectives usually have an initial capital letter and are derived
from proper nouns such as country names etc.

Yunnan Laˇhuˍ Yunnan Lahu ‘Lahu of Yunnan’

Yanˇ chaw Karen person ‘Karen people’

# Descriptive

sick child ‘a sick child’

boy ‘a brave boy’

# Quantatative

\[sec:quantatative\] These adjectives are used for describing amounts
that cannot generally be measured or counted.

yawˇ awˬ caˇ maˇ ve he rice eat much ‘he ate a lot’

yawˇ [a ciˉ]{} tiˉ [caˆ ve]{} he [a little]{} only eat ‘he only ate a
little’

yawˇ [te chiˉ]{} maˇ caˇ he nothing not eat ‘he ate nothing’

yawˇ caˇ [law ve]{} he eat enough ‘he ate enough’

yawˇ [hk’a peu-eˬ]{} [caˇ ve]{} he all eat ‘he ate \[it\] all’

# Demonstrative

These are used to indicate a particular object or noun or to distinguish
one noun from another.

nuˇ chi cow this ‘this cow’

i mvuhˇ oˇ ve horse that ‘that horse’

g’aˆ chi teˇ hpaˍ chicken this plural ‘these chickens’

a pehˬ oˇ ve teˇ hpaˍ duck that plural ‘those ducks’

\[ex:no\] noˇ ve nuˇ teˇ hkeh that cow plural ‘those cows over there’

\[ex:mo\] moˇ ve yehˬ that house ‘that house over there’

Notice that Lahu has a demonstrative for ‘this’, ‘that’ and ‘over
there’. Example \[ex:no\] and \[ex:mo\] both indicate ‘over there’ or
‘yonder’ with the words ‘noˇ’ and ‘moˇ’ respectively. The difference
between them being that ‘noˇ’ refers to things *higher* and ‘moˇ’ refers
to things *lower*.[^1]

# Distributive

\[sec:distributive\] Distributative adjectives are used to describe
amounts that could be counted as apposed to some of the quantatative
adjectives in section \[sec:quantatative\] which are unmeasureable. Most
of these adjectives require classifiers which are described in
chapter \[sec:classifiers\].

## Every {#every .unnumbered}

To denote ‘every’ the phrase ‘teˇ …le le’ is wrapped around the
classifier.

teˇ g’aˇ [le le]{} [teˇ]{} (clf) [le le]{} ‘all people’

teˇ kaˬ [le le]{} [teˇ]{} (clf) [le le]{} ‘every place’

teˇ nyi [hkanˍ teˇ]{} nyi teˇ (clf) [hkanˍ teˇ]{} (clf) ‘every other
day’

## Some {#some .unnumbered}

To denote ‘some’ the phrase ‘teˇ’ and a repeated classifier is used.

teˇ g’aˇ g’aˇ [teˇ]{} (clf) (clf) ‘some people’

## Only {#only .unnumbered}

To denote ‘only’ the word ‘tiˉ’ is used after the classifier.

teˇ g’aˇ tiˉ [teˇ]{} (clf) [tiˉ]{} ‘only one person’

shehˆ nyi tiˉ three (clf) only ‘only three days’

# Numeral

To describe a specific number of nouns in Lahu you need to use
classifiers. These are discussed in more detail in
chapter \[sec:classifiers\].

Chaw [shehˆ chi]{} g’aˇ [shaˍ g’a ve yoˬ]{} man thirty (clf) hunt
‘Thirty men went hunting’

For a more indefinite or general amount the phrase ‘aˬ laˬ’ (about) or
‘aˬ laˬ hk’e’ (nearly) can be added.

Chaw [aˬ laˬ]{} [shehˆ chi]{} g’aˇ [shaˍ g’a ve yoˬ]{} man about thirty
(clf) hunt About thirty men went hunting

[^1]: This is due to the Lahu people mainly residing on hillsides where
    ‘up’ and ‘down’ are descriptions used very often
