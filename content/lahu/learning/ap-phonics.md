The following tables show the differences between the four main
orthographies developed for the Lahu language. The *Protestant*
orthography is given priority as it is the most widely used.

   Protestant   Matisoff   Catholic   Chinese
  ------------ ---------- ---------- ---------
       a           a          a          a
       i           i          i          i
      u/uh         u         u/ö         u
       e           e          e          e
       o           o          o          o
       eh                     è         ie
       aw                     ò         aw
     ui/uh                   ü/ö        eu
       eu                     ë         eu
       ai          ay         ai        ai
       ao          aw         ao        ao
      u-i          wi                
      o-e          we                
      aweh         w                 

  : \[tab:vow-orth-comp\]Vowel Orthographic Comparison (adapted from
  Matisoff[@JAM:2006])

   Protestant   Matisoff   Catholic   Chinese
  ------------ ---------- ---------- ---------
       k’          q          q          q
      hk’          qh         qh        qh
       k           k          k          k
       hk          kh         kh        kh
       g           g          g          g
       ng                     ng        ng
      c/tc         c         c/tc       c/z
     ch/ts         ch       ch/ts      ch/zh
      j/dz         j         j/dz      j/dz
       t           t          t          t
       ht          th         th        th
       d           d          d          d
      n/ny         n         n/gn        n
      p/pf         p         p/pf        p
     hp/hpf        ph       ph/phf      ph
      b/bv         b         b/bv        b
      m/mv         m         m/mv        m
       h           h          h          h
       g’                     gh         x
      sh/s         s         sh/s      sh/s
      y/z          y         y/z        y/r
       f           f          f          f
       v           v          v          v
       l           l          l          l

  : \[tab:cons-orth-comp\]Consonant Orthographic Comparison (adapted
  from Matisoff[@JAM:2006])

   Protestant   Matisoff   Catholic   Chinese
  ------------ ---------- ---------- ---------
       ca          ca        caˍ        ca
      caˉ          cá         ca        caq
      caˇ          câ         ca        cad
      caˬ          cà        caˬ        cal
      caˍ          cā         ca        cal
      caˆ         câ?        caˆ        cat
      ca         cà?        ca        car

  : \[tab:tone-orth-comp\]Tonal Orthographic Comparison (adapted from
  Matisoff[@JAM:2006])


