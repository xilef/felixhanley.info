Adverbs (words that describe a verb) appear immediately before a verb.
There are four types of adverbs in Lahu [@PL:1986] as follows.

## Negative adverbs {#negative-adverbs .unnumbered}

We have seen these in \[sec:negation\] but here are a few more examples:

maˇ te not do

taˇ te don’t do

## Positive adverbs {#positive-adverbs .unnumbered}

These have a positive sense. Some examples:

te [with all strength]{} do

feu kuˬ ve [with all strength]{} intensely call

haˆ te quickly do

## *hk’a* adverbs {#hka-adverbs .unnumbered}

The word ‘hk’a’ means *all* but can be combined with other verbs to
create adverbial expressions such as the following.

hk’a biˇ keu ve fully [to fill]{} ‘to fill fully’

hk’a bvuhˆ caˇ ve [to be full]{} eat ‘to eat to the full’

hk’a law caˉ ve sufficient [to cook]{} ‘to cook sufficiently’

## *-eh* adverbs {#eh-adverbs .unnumbered}

The syllable ‘-eh’ could be called an adverbalising particle as it can
be added to the end of words to make them an adverb.

nawˉ green nawˉ-eh greenish

ba-eh brightly
