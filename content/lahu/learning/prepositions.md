Prepositions indicate a relation between things mentioned in a sentence.
Commong prepositions in English are *on*, *above*, *below*, *before*,
*after* etc.. Prepositions come in a number of varieties but here we
will discuss just *place* and *time*.

# Place

[Some prepositions of place]{}

in

between

around

on

next to

under

left

right

in front

behind

# Time

[Some prepositions of place]{}

after

before

now
