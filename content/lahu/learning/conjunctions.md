[Common Conjunctions]{}

and

but

if

when

even though

becuase

but

therefore

so

actually

becuase of that

on behalf of (for)

Conjuctions are words or phrases that join words or phrases in a
sentence. Some examples are the words ‘but’, ‘and’ and ‘or’. Two types
of conjuctions are common in Lahu. They are commonly called
’Coordinating’ and ’Subordinate’ conjuctions.

# Coordinating Conjuctions {#coordinating-conjuctions .unnumbered}

These are conjuctions that join two phrases in a sentence that equal in
importance or are not dependent on each other. Examples of coordinating
conjunctions are ‘and’, ‘or’ and ‘but’.

so we sad ‘So, we were sad’

yawˇ maˇ k’ai sheˍ but he not go yet ‘But he hasn’t gone yet’

# Subordinate Conjunctions {#subordinate-conjunctions .unnumbered}

A subordinate conjuction joins two clauses (or phrases), one being
subordinate or dependent on the other. These conjuctions can be a little
more difficult to get right. The order of a sentence in Lahu is
different to the same sentence in English when using subordinate
cunjunctions. The following examples show subordinate conjuction use in
English.

‘If [has has money]{} [he will go]{}’ (conjuction) [(subordinate
clause)]{} [(main clause)]{}

‘[He will go]{} if [he has money]{}’ [(main clause)]{} (conjuction)
[(subordinate clause)]{}

In the above examples, the sentence can be structured in two different
orders. In Lahu, however, the subordinate clause must always precede the
main clause.

\[ex:subord-conj\] k’o, [k’ai tuˬ yoˬ]{} [he money have]{} if [go
will]{} [(subordinate clause)]{} (conjuction) [(main clause)]{} ‘If he
has money, he will go’

One technique to get the sentence in the correct order is to start with
the conjunction when saying the sentence in English. The resulting two
clauses of the sentence are then in the correct order. So instead of
thinking like this: think in this order: The meanings are the same in
English but you now have the clauses in the correct order for Lahu. All
that is needed, then, is to place the conjuction (in this case ‘if’)
between the two clauses. See example \[ex:subord-conj\]. Examples of
subordinate conjunctions are ‘if’, ‘for’, ‘even though’ and ‘because’.

Here are some more examples:

\[ex:conj-first\] Ngaˬ [bvuh ve]{} k’o, nawˬ [g’aw ve yoˬ]{} I write if
you read ‘If I write, you read’ ‘You read if I write’

Nawˬ yaw htaˇ… you spoke when ‘When you spoke…’

Nawˬ [yaw ve]{} htawˇ, ngaˬ maˇ kaˇ you speak [even though]{} I not hear
‘Even though you speak, I don’t hear \[you\]’

Yawˇ [ngaˬ hui]{} hta [maˍ laˇ ve]{} [pa taw]{}, [ngaˬ hui]{} [ha lehˬ
ve yoˬ]{} \[he\] us (par) teach because we happy ‘We’re happy because
\[he\] taught us’

yawˇ maˇ k’ai sheˍ but \[he\] not go yet ‘But \[he\] hasn’t gone yet’

ngaˬ caˇ maˇ g’a therefore I each not can ‘Therefore I cannot eat’

so we sad ‘So we are depressed’

yawˇ maˇ [na yuˬ]{} actually \[he\] not listen ‘Actually, \[he\] did not
listen’

nyiˇ hkeh [oˇ ve]{} suh [peuˬ ve yoˬ]{} [because of that]{} horse two
(clf) those die complete ‘Because of that those two horses died’

\[ex:in-order-to\] Te tuˬ, yawˇ [awˬ chawˇ]{} shehˆ g’aˇ hta g’a [ca ve
yoˬ]{} do [in order to]{} \[he\] friend three (clf) (par) must find ‘In
order to do it, \[he\] must find three friends’

\[ex:conj-last\] Yawˇ [awˬ pon]{} te sheˍ \[he\] [on behalf of]{} do
(par) ‘Do it for \[him\]’

Notice in example \[ex:in-order-to\] that the word ‘tuˬ’ is the same
word used for future tense (see \[sec:future\]). For the conjuction ‘in
order to’ it is used in the middle of a sentence.
