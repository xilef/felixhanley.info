---
title: Work
description: Current employment
date: 2016-09-02
menu:
  main:
    identifier: work
    weight: 10
---

Some of the more interesting projects I have worked on:

### ARKpX / Lockbox

Data protection and sharing using strong, client side encryption with
decentralised key management.

I was the lead programmer for the core Javascript library which contained the PKI and
business logic used by all client software. This included the ASN.1
schema generation and validation, encryption primitives and fallbacks
for browser deficiencies, and the key management logic.

This core library is used by the Web UI, the iOS app and is being
integrated into the desktop client. It is written in 'vanilla Javascript' with full unit and integration tests.

- Client: KL Data security
- URL: http://arkpx.com
- Role: Senior Software Engineer


### Ayuda Hosting

A Melbourne based hosting platform provider with a large client base
and a good developer/designer relationships.

With a small team I was involved in all aspects of the hosting and
client development business. This included the configuration and
security of all servers, both bare metal and virtual; development of
the custom billing and CMS software; redundant network configuration, monitoring and security.

I also played a large role in the consulting and system design for various client projects seeing them from initial meetings to project handover.

- Client: Ayuda hosting
- URL: http://ayudahosting.com.au
- Role: Programmer / Network Engineer
