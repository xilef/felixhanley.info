---
title: Projects
date: 2011-01-01
lastmod: 2017-03-11
menu:
  main:
    identifier: projects
    weight: 30
---

Projects that I am actively working on or have contributed to. You may find
others on [GitHub](https://github.com/felix/) and perhaps even
[Bitbucket](https://bitbucket.org/xilef/)

## Open Source Contributions

### [ASN.1 Decoder/Encoder/DSL in Javascript](https://github.com/felix/asn1.js)

A very useful project for working with ASN.1 and encryption primitives with
Javascript. This project was chosen over others due to its good handling of
implicit types and encapsulated models. It also has a nice DSL.

### [FreeBSD Ports](https://github.com/felix/freebsd-ports)

Mainly contributions to Postgresql and MariaDB Galera clustering support for
client work.

### [Void Linux](https://github.com/felix/void-packages)

As Linux distros go this one is very similar to the BSD method of building
ports and it is not ham-strung by systemd. Used where BSD is not supported by
hardware.

## Other Projects
