---
title: My Family Tree
description: Family Tree of Felix Hanley
date: 2011-03-07
menu:
  main:
    parent: projects
    weight: 30
---

Along with my mother (mainly her!) I have been building my family tree.

<!--more-->

It is currently being maintained using [Gramps](http://gramps-project.org/) and
can be view on [my family tree page](http://genealogy.felixhanley.info/)
